// Create a variable that will store the require method to import "http" package
const http = require("http");

// Assign the port variable to 4000
const port = 4000;

// Create a server variable that will store the http object and createServer method
const server = http.createServer(function(request, response) {


	if (request.url == "/" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to the Booking System");
	}

	if (request.url == "/profile" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to your profile");
	}

	if (request.url == "/courses" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here's our courses available");
	}

	if (request.url == "/addCourse" && request.method == "POST") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Add a course to our resources");
	}

	if (request.url == "/updateCourse" && request.method == "PUT") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update a course to our resources");
	}

	if (request.url == "/archiveCourse" && request.method == "DELETE") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Archive courses to our resources");
	}
})

// 
server.listen(port);

console.log(`Server is running at localhost: ${port}`);